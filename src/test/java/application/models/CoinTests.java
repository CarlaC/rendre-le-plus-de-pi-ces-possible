package application.models;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import org.junit.Test;

public class CoinTests {
	private static double EPSILON = 0.0001;
	/**
	 * TO_STRING() TESTS
	 */
	@Test
	public void test1ECoinToString_shouldBeOk() {
		assertEquals("1 euro", Coin.ONE_EURO.toString());
	}

	@Test
	public void test2ECoinToString_shouldBeOk() {
		assertEquals("2 euros", Coin.TWO_EUROS.toString());
	}
	
	@Test
	public void test50CCoinToString_shouldBeOk() {
		assertEquals("50 centimes", Coin.FIFTY_CENTS.toString());
	}
	
	@Test
	public void test20CCoinToString_shouldBeOk() {
		assertEquals("20 centimes", Coin.TWENTY_CENTS.toString());
	}
	
	@Test
	public void test10CCoinToString_shouldBeOk() {
		assertEquals("10 centimes", Coin.TEN_CENTS.toString());
	}
	
	@Test
	public void test5CCoinToString_shouldBeOk() {
		assertEquals("5 centimes", Coin.FIVE_CENTS.toString());
	}
	
	@Test
	public void test2CCoinToString_shouldBeOk() {
		assertEquals("2 centimes", Coin.TWO_CENTS.toString());
	}
	
	@Test
	public void test1CCoinToString_shouldBeOk() {
		assertEquals("1 centime", Coin.ONE_CENTS.toString());
	}
	
	/**
	 * COMPARE_COINS TESTS
	 */
	@Test
	public void testCompareCoins_shouldBeLower() {
		assertEquals(-1, Coin.FIFTY_CENTS.compareCoins(Coin.ONE_EURO));
	}
	
	@Test
	public void testCompareCoins_shouldBeEqual() {
		assertEquals(0, Coin.FIFTY_CENTS.compareCoins(Coin.FIFTY_CENTS));
	}
	
	@Test
	public void testCompareCoins_shouldBeGreater() {
		assertEquals(1, Coin.ONE_EURO.compareCoins(Coin.FIFTY_CENTS));
	}
	
	@Test
	public void testCompareCoinsNull_shouldBeGreater() {
		assertEquals(1, Coin.ONE_EURO.compareCoins(null));
	}
	
	/**
	 * GET_MIN_OF TESTS
	 */
	@Test
	public void testGetMinOf_shouldReturn50C() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.FIFTY_CENTS);
		coins.add(Coin.ONE_EURO);
		
		final Optional<Coin> response = Coin.getMinOf(coins);
		assertFalse(response.isEmpty());
		assertEquals(Coin.FIFTY_CENTS, response.get());
	}
	
	@Test
	public void testGetMinOf_shouldReturn10C() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.TWO_EUROS);
		coins.add(Coin.ONE_EURO);
		coins.add(Coin.TEN_CENTS);
		
		final Optional<Coin> response = Coin.getMinOf(coins);
		assertFalse(response.isEmpty());
		assertEquals(Coin.TEN_CENTS, response.get());
	}
	
	@Test
	public void testGetMinOfSingleton_shouldReturn1E() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.ONE_EURO);

		final Optional<Coin> response = Coin.getMinOf(coins);
		assertFalse(response.isEmpty());
		assertEquals(Coin.ONE_EURO, response.get());
	}
	
	@Test
	public void testGetMinOfNull_shouldReturnEmptyOpt() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.ONE_EURO);

		final Optional<Coin> response = Coin.getMinOf(null);
		assertTrue(response.isEmpty());
	}
	
	/**
	 * GET_MAX_OF TESTS
	 */
	@Test
	public void testGetMaxOf_shouldReturn1E() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.FIFTY_CENTS);
		coins.add(Coin.ONE_EURO);
		
		final Optional<Coin> response = Coin.getMaxOf(coins);
		assertFalse(response.isEmpty());
		assertEquals(Coin.ONE_EURO, response.get());
	}
	
	@Test
	public void testGetMaxOf_shouldReturn2E() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.TWO_EUROS);
		coins.add(Coin.ONE_EURO);
		coins.add(Coin.TEN_CENTS);
		
		final Optional<Coin> response = Coin.getMaxOf(coins);
		assertFalse(response.isEmpty());
		assertEquals(Coin.TWO_EUROS, response.get());
	}
	
	@Test
	public void testGetMaxOfSingleton_shouldReturn1E() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.ONE_EURO);

		final Optional<Coin> response = Coin.getMaxOf(coins);
		assertFalse(response.isEmpty());
		assertEquals(Coin.ONE_EURO, response.get());
	}
	
	@Test
	public void testGetMaxOfNull_shouldReturnEmptyOpt() {
		final Set<Coin> coins = new HashSet<>();
		coins.add(Coin.ONE_EURO);

		final Optional<Coin> response = Coin.getMaxOf(null);
		assertTrue(response.isEmpty());
	}
	
	/**
	 * SUM_COINS TESTS
	 */
	@Test
	public void testSumCoinsOfEmpty_shouldBe0() {
		assertEquals(0.0, Coin.sumCoins(new HashMap<>()), EPSILON);
	}
	
	@Test
	public void testSumCoinsOfNull_shouldBe0() {
		assertEquals(0.0, Coin.sumCoins(null), EPSILON);
	}
	
	@Test
	public void testSumCoins_shouldBe0() {
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.ONE_EURO, 0);
		
		assertEquals(0.0, Coin.sumCoins(coins), EPSILON);
	}
	
	@Test
	public void testSumCoins_shouldBe2() {
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.ONE_EURO, 2);
		
		assertEquals(2.0, Coin.sumCoins(coins), EPSILON);
	}
	
	@Test
	public void testSumCoins_shouldBe3() {
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.ONE_EURO, 1);		
		coins.put(Coin.TWO_EUROS, 1);
		
		assertEquals(3.0, Coin.sumCoins(coins), EPSILON);
	}
	
	@Test
	public void testSumCoins_shouldBe51C() {
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.FIFTY_CENTS, 1);		
		coins.put(Coin.ONE_CENTS, 1);
		
		assertEquals(0.51, Coin.sumCoins(coins), EPSILON);
	}
}
