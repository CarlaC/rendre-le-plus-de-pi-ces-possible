package application;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import application.Main;
import application.models.Coin;

public class MainTests {
	@Test
	public void testComputeChange_shouldBe100cents() {
		final int change = 100;
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.ONE_EURO, 1);
		coins.put(Coin.FIFTY_CENTS, 2);
		coins.put(Coin.TWO_EUROS, 1);
		coins.put(Coin.TWENTY_CENTS, 5);
		coins.put(Coin.TEN_CENTS, 10);
		coins.put(Coin.FIVE_CENTS, 20);
		coins.put(Coin.TWO_CENTS, 50);
		coins.put(Coin.ONE_CENTS, 100);

		final Map<Coin, Integer> gotBack = Main.computeChange(change, coins);
		assertEquals(1, gotBack.size());
		assertTrue(gotBack.get(Coin.ONE_CENTS) == 100);
	}
	
	@Test
	public void testComputeChange_shouldBe1E1cents() {
		final int change = 101;
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.ONE_EURO, 1);
		coins.put(Coin.ONE_CENTS, 100);

		final Map<Coin, Integer> gotBack = Main.computeChange(change, coins);
		assertEquals(2, gotBack.size());
		assertTrue(gotBack.get(Coin.ONE_CENTS) == 1);
		assertTrue(gotBack.get(Coin.ONE_EURO) == 1);
	}
	
	@Test
	public void testComputeChange_shouldBe1E() {
		final int change = 100;
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.TWO_CENTS, 1);
		coins.put(Coin.FIVE_CENTS, 1);
		coins.put(Coin.TEN_CENTS, 1);
		coins.put(Coin.TWENTY_CENTS, 2);
		coins.put(Coin.FIFTY_CENTS, 1);
		coins.put(Coin.ONE_EURO, 3);
		coins.put(Coin.TWO_EUROS, 1);

		final Map<Coin, Integer> gotBack = Main.computeChange(change, coins);
		assertEquals(3, gotBack.size());
		assertTrue(gotBack.get(Coin.TWENTY_CENTS) == 2);
		assertTrue(gotBack.get(Coin.TEN_CENTS) == 1);
		assertTrue(gotBack.get(Coin.FIFTY_CENTS) == 1);
	}

	@Test
	public void testComputeNegativeChange_shouldBeEmpty() {
		final int change = -1;
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.TWO_EUROS, 1);

		final Map<Coin, Integer> gotBack = Main.computeChange(change, coins);
		assertTrue(gotBack.isEmpty());
	}
	
	@Test
	public void testComputeImpossible1_shouldBeEmpty() {
		final int change = 1;
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.TWO_EUROS, 1);

		final Map<Coin, Integer> gotBack = Main.computeChange(change, coins);
		assertTrue(gotBack.isEmpty());
	}
	
	@Test
	public void testComputeImpossible2_shouldBeEmpty() {
		final int change = 100;
		final Map<Coin, Integer> coins = new HashMap<>();
		coins.put(Coin.TEN_CENTS, 1);

		final Map<Coin, Integer> gotBack = Main.computeChange(change, coins);
		assertTrue(gotBack.isEmpty());
	}
	
	@Test
	public void testComputeChangeWithNoCoins_shouldBeEmpty() {
		final int change = 100;
		final Map<Coin, Integer> coins = new HashMap<>();

		final Map<Coin, Integer> gotBack = Main.computeChange(change, coins);
		assertTrue(gotBack.isEmpty());
	}
}
