package application.models;

import java.util.Map;
import java.util.Optional;
import java.util.Set;

public enum Coin {
	ONE_EURO(100), TWO_EUROS(200), FIFTY_CENTS(50), TWENTY_CENTS(20), TEN_CENTS(10), FIVE_CENTS(5), TWO_CENTS(2),
	ONE_CENTS(1);

	private int value;

	private Coin(final int value) {
		this.value = value;
	}

	public int getValue() {
		return this.value;
	}

	@Override
	public String toString() {
		if (this.value >= 100) {
			return String.format("%d euro%s", this.value / 100, (this.value / 100 > 1 ? 's' : ""));
		} else {
			return String.format("%d centime%s", this.value, (this.value > 1 ? 's' : ""));
		}
	}

	public int compareCoins(final Coin coin) {
		if (coin == null) {
			return 1;
		}

		return Integer.compare(this.getValue(), coin.getValue());
	}

	public static Optional<Coin> getMinOf(final Set<Coin> coins) {
		if (coins == null) {
			return Optional.empty();
		}

		return coins.stream().min(Coin::compareCoins);
	}

	public static Optional<Coin> getMaxOf(final Set<Coin> coins) {
		if (coins == null) {
			return Optional.empty();
		}

		return coins.stream().max(Coin::compareCoins);
	}

	public static int sumCoins(final Map<Coin, Integer> coins) {
		if (coins == null || coins.isEmpty()) {
			return 0;
		}
		
		return coins.keySet().stream().map(coin -> coin.getValue() * coins.get(coin)).reduce(0, Integer::sum);
	}
}
