package application;

import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.Map;
import java.util.Scanner;

import application.models.Coin;

/**
 * Console application to compute change with the most possible coins. Based on
 * recursive algorithm : 
 * IF nothing to give back THEN 
 * 		STOP 
 * ELSE 
 * 		DO pick up all smallest value coins 
 * 		WHILE sum of picked up coins is lower than change to give back
 * 		SUBTRACT greatest coin of picked up coins to remaining change to give back
 * 		REMOVE this coin from picked up coins 
 * 		START AGAIN with remaining change to give back and picked up coins
 * 
 * @author Carla Candiotti
 */
public class Main {
	public static void main(String[] args) {
		final Scanner scan = new Scanner(System.in);
		System.out.print("Entrer la monnaie � rendre :");

		// Input change
		double change = 0.0;
		while (change == 0.0) {
			try {
				change = scan.nextDouble();
			} catch (InputMismatchException ex) {
				System.out.print("Il faut respecter le format x,xx.");
				change = 0.0;
				scan.nextLine();
			}
		}

		// Input available coins
		if (change > 0.0) {
			final Map<Coin, Integer> availableCoins = new HashMap<>();
			System.out.println("Entrer les pi�ces disponibles :");

			for (Coin coin : Coin.values()) {
				int numberEntered = -1;
				while (numberEntered < 0) {
					try {
						System.out.print(String.format("Pi�ces de %s:", coin.toString()));
						numberEntered = scan.nextInt();
					} catch (InputMismatchException ex) {
						System.out.println("Veuillez entrer un nombre entier.");
						numberEntered = -1;
						scan.nextLine();
					}
				}

				if (numberEntered > 0) {
					availableCoins.put(coin, numberEntered);
				}
			}
			
			final int integerChange = (int)(change * 100);

			if (isImpossible(integerChange, availableCoins)) {
				System.out.println("Impossible de rendre la monnaie !");
			} else {
				final Map<Coin, Integer> gotCoins = computeChange(integerChange, availableCoins);
				if (gotCoins.isEmpty()) {
					System.out.println("Impossible de rendre la monnaie !");
				} else {
					System.out.println("Monnaie rendue :");
					for (Coin coin : gotCoins.keySet()) {
						System.out.println(String.format("%d pi�ce%s de %s", gotCoins.get(coin),
								gotCoins.get(coin) > 1 ? 's' : "", coin.toString()));
					}
				}
			}
		} else {
			System.out.print("Veuillez r�essayer.");
		}

		scan.close();
	}

	/**
	 * Gives back the most possible coins.
	 * 
	 * @param restToGiveBack remaining change to give back
	 * @param availableCoins remaining available coins
	 * @return computed change
	 */
	public static Map<Coin, Integer> computeChange(final int restToGiveBack,
			final Map<Coin, Integer> availableCoins) {
		if (isImpossible(restToGiveBack, availableCoins)) {
			return new HashMap<Coin, Integer>();
		}

		final Map<Coin, Integer> selectionnedCoins = new HashMap<Coin, Integer>();
		do {
			// As availableCoins cannot be empty, there is inevitably a minimum coin
			final Coin smallestCoin = Coin.getMinOf(availableCoins.keySet()).get();
			selectionnedCoins.put(smallestCoin, availableCoins.remove(smallestCoin));
		} while (Coin.sumCoins(selectionnedCoins) < restToGiveBack && !availableCoins.isEmpty());

		// As selectionnedCoins cannot be empty, there is inevitably a maximum coin
		final Coin greatestCoin = Coin.getMaxOf(selectionnedCoins.keySet()).get();
		final int numberOfGreatestCoin = selectionnedCoins.get(greatestCoin);

		// Picking greatestCoin from selectionnedCoins
		if (numberOfGreatestCoin < 2) {
			selectionnedCoins.remove(greatestCoin);
		} else {
			selectionnedCoins.put(greatestCoin, numberOfGreatestCoin - 1);
		}

		// Recursive call
		final Map<Coin, Integer> coins = computeChange(restToGiveBack - greatestCoin.getValue(), selectionnedCoins);

		coins.putIfAbsent(greatestCoin, 0);
		coins.put(greatestCoin, coins.get(greatestCoin) + 1);

		return coins;
	}

	public static boolean isImpossible(final int restToGiveBack, final Map<Coin, Integer> availableCoins) {
		return restToGiveBack <= 0 || availableCoins.isEmpty() || Coin.sumCoins(availableCoins) < restToGiveBack
				|| (availableCoins.size() == 1
						&& Coin.getMinOf(availableCoins.keySet()).get().getValue() > restToGiveBack);
	}
}
